#ifndef FENETRE_H
#define FENETRE_H

#include <SDL2/SDL.h>
#include <vector>
#include <stdio.h>

#include "item.h"
#include "case.h"

class Fenetre
{
private:
    SDL_Window* m_fenetre;
    SDL_Renderer* m_renderer;
    std::vector<SDL_Texture*> m_items;
    int m_largeur;
    int m_longueur;
public:
    Fenetre(int largeur, int longueur);
    void ajoutImg(Item* ajoute);
    ~Fenetre();
};

#endif // FENETRE_H
