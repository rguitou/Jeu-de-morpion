#include "case.h"

Case::Case(const int milieuX, const int milieuY, int index)
    :Item(), m_milieuX(milieuX), m_milieuY(milieuY), m_index(index)
{
    m_tailleXY = 300;
    calcCoord();
    creerRectangle();
}

void Case::calcCoord()
{
    switch (m_index % 3) {
    case 0:
        m_X =( m_milieuX - (m_tailleXY * 1.5)) + 1;
        break;
    case 1:
        m_X = (m_milieuX - (m_tailleXY / 2)) + 1;
        break;
    case 2:
        m_X = (m_milieuX + (m_tailleXY / 2)) + 1;
        break;
    }

    switch (m_index / 3) {
    case 0:
        m_Y = (m_milieuY - (m_tailleXY * 1.5)) + 1;
        break;
    case 1:
        m_Y = (m_milieuY - (m_tailleXY / 2)) + 1;
        break;
    case 2:
        m_Y = (m_milieuY + (m_tailleXY / 2)) + 1;
        break;
    }

}

void Case::creerRectangle()
{
    m_case = new SDL_Rect({m_X, m_Y, m_tailleXY - 2, m_tailleXY - 2});

    SDL_FillRect(m_representation, m_case, SDL_MapRGB(m_representation->format, 255, 255, 255));
}

Case::~Case()
{
    delete m_case;
}
