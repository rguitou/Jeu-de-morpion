#include "item.h"

SDL_Surface* FOND = IMG_Load("fond.jpg");

Item::Item()
{
    m_representation = FOND;
    if(!m_representation)
    {
        printf("Erreur de chargement de l'image : %s",SDL_GetError());
    }
}


SDL_Surface* Item::getRepresentation()
{
    return m_representation;
}
