#include "jeton.h"

Jeton::Jeton(SDL_Renderer* rn, int rayon)
{
    creerCercle(rn, rayon);
}

void Jeton::creerCercle(SDL_Renderer* rn, int cx, int cy, int rayon, int coul)
{
    m_case =

    SDL_FillRect(m_representation, m_case, SDL_MapRGB(m_representation->format, 255, 255, 255));

    int d, y, x;

    d = 3 - (2 * rayon);
    x = 0;
    y = rayon;

    while (y >= x) {
      SDL_RenderDrawLine(rn ,cx - x, cy - y, 2 * x + 1, coul);
      SDL_RenderDrawLine(rn ,cx - x, cy + y, 2 * x + 1, coul);
      SDL_RenderDrawLine(rn, cx - y, cy - x, 2 * y + 1, coul);
      SDL_RenderDrawLine(rn, cx - y, cy + x, 2 * y + 1, coul);

      if (d < 0)
        d = d + (4 * x) + 6;
      else {
        d = d + 4 * (x - y) + 10;
        y--;
      }

      x++;
}
