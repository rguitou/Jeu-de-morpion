 #include "fenetre.h"
#include "evenement.h"


Fenetre::Fenetre(int largeur, int longueur)
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0 )
    {
        fprintf(stdout,"Échec de l'initialisation de la SDL (%s)\n",SDL_GetError());
    }
    else
    {
        /* Création de la fenêtre */
        m_fenetre = SDL_CreateWindow("Bite",SDL_WINDOWPOS_UNDEFINED,
                                     SDL_WINDOWPOS_UNDEFINED,
                                     largeur,
                                     longueur,
                                     SDL_WINDOW_SHOWN);
        m_renderer = SDL_CreateRenderer(m_fenetre, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED );

        Item i;

        ajoutImg(&i);
        SDL_RenderPresent(m_renderer);

        for(int i = 0; i < 9; i++)
        {
           Case c(largeur / 2, longueur / 2, i);
           ajoutImg(&c);
        }

        SDL_RenderPresent(m_renderer);

        if(! m_fenetre )
        {
            fprintf(stderr,"Erreur de création de la fenêtre: %s\n",SDL_GetError());
        }
        Evenement gestion(m_fenetre);
    }

}

void Fenetre::ajoutImg(Item* ajoute)
{
    SDL_Texture* texture = SDL_CreateTextureFromSurface(m_renderer, ajoute->getRepresentation());
    m_items.push_back(texture);
    SDL_RenderCopy(m_renderer, texture, NULL, NULL);
}

Fenetre::~Fenetre()
{
    for(SDL_Texture* t : m_items)
    {
        SDL_DestroyTexture(t);
    }

    SDL_DestroyRenderer(m_renderer);
    SDL_DestroyWindow(m_fenetre);
}
