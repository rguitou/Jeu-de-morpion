#ifndef CASE_H
#define CASE_H

#include <SDL2/SDL.h>

#include "item.h"
#include "case.h"


class Case : public Item
{
private:
    const int m_milieuX;
    const int m_milieuY;
    int m_tailleXY;
    int m_index;
    SDL_Rect* m_case;
    int m_X;
    int m_Y;
public:
    Case(const int milieuX, const int milieuY, int index);
    void calcCoord();
    void creerRectangle();
    ~Case();
};
#endif // CASE_H
