#ifndef JETON_H
#define JETON_H


#include "item.h"
#include "case.h"

#include <SDL2/SDL.h>

class Jeton : public Item
{
    SDL_Color m_couleurJeton;
    int m_rayon;
public:
    Jeton(int radius);
    void creerCercle(int cx, int cy, int rayon, int coul);
};

#endif // JETON_H
