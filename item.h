#ifndef ITEM_H
#define ITEM_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>

class Item
{
protected:
    SDL_Surface* m_representation;
public:
    Item();
    SDL_Surface* getRepresentation();
};

#endif // ITEM_H
