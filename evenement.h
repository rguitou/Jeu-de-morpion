#ifndef EVENEMENT_H
#define EVENEMENT_H

#include "fenetre.h"

class Evenement
{
    SDL_Window* m_fenetreCourante;
    int m_sourisX;
    int m_sourisY;
public:
    Evenement(SDL_Window* fenetre);
};

#endif // EVENEMENT_H
