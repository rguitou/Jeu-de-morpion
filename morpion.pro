TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    fenetre.cpp \
    item.cpp \
    case.cpp \
    joueur.cpp \
    jeton.cpp \
    evenement.cpp

INCLUDEPATH += /usr/local/include

LIBS += -L/usr/lib -lSDL2 -lSDL2_image

HEADERS += \
    fenetre.h \
    item.h \
    case.h \
    joueur.h \
    jeton.h \
    evenement.h
